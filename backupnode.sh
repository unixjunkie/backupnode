#!/usr/bin/env bash
# backupnode 0.1
# Copyright 2016 unixjunkie
# License : GPLv2
# backupnode comes with ABSOLUTELY NO WARRANTY; This is free software, and you are welcome to redistribute it under certain conditions
backup_path=/path/to/backups
if [ x"$1" != "x" ]
then
	if [ -d $backup_path ]
	then
		nodename=$1
		now=$(date "+%Y%j_%Y-%m-%d")
		out=${backup_path}/${nodename}_${now}.txz
		cmd="tar -C / -Jcf ${out} root/ home/ var/log/ var/lib/mysql/ var/spool/ var/lib/sudo/ var/lib/php/ var/backups/ var/www/ usr/local/ opt/"
		cmdv="tar -Jtf ${out}"
		cmdi="ls -lhtr ${backup_path}/"
		echo "_%%"
		echo "$nodename : $now : $cmd : $out"
		echo "%%"
		echo "backing up ${nodename}"
		echo
		$cmd
		echo
		echo "%%"
		echo "verifying ${nodename}"
		echo
		$cmdv
		echo
		echo "%%"
		echo "backup info on ${nodename}"
		echo
		$cmdi
		echo
		echo "%%_"
		
	else
		echo "Backup path not found, or not set in the script"
		exit 1
	fi
else
	echo "Please provide name (no spces)"
	exit 1
fi